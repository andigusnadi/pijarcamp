{{--
<link rel="stylesheet" href="{{ asset('dist/css/datatable/dataTables.bootstrap5.min.css') }}" />
<link rel="stylesheet" href="{{ asset('dist/css/datatables.min.css') }}" /> --}}

@extends('layout.Template_admin')
@section('title', 'Dashboard')
@section('content')

{{-- DATA TABLE --}}

<div class="card card-preview">
    <div class="card-inner">
        <table class="table table-striped table-bordered" id="myTable" style="width:100%"
            aria-label="pengguna">
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">NAMA PRODUK</th>
                    <th class="text-center">KETERANGAN</th>
                    <th class="text-center">HARGA</th>
                    <th class="text-center">JUMLAH</th>
                    <th class="text-center">VIEW</th>
                    <th class="text-center">EDIT</th>
                    <th class="text-center">DELETE</th>
                </tr>
            </thead>
            <tbody>
                @php
                $no = 1;
                @endphp
                @foreach ($ListProduk as $data)
                <tr class="">
                    <td class="text-center">{{$no++}}</td>
                    <td class="text-center">{{$data->nama_produk}}</td>
                    <td class="text-center">{{$data->keterangan}}</td>
                    <td class="text-center">{{$data->harga}}</td>
                    <td class="text-center">{{$data->jumlah}}</td>
                    <td class="form-group">
                        <a class="btn btn-info" id="set_dtl_data_view" data-toggle="modal" data-target="#modal_view"
                            data-id_produk="{{$data->id}}" data-nama_produk="{{$data->nama_produk}}"
                            data-keterangan="{{$data->keterangan}}" data-harga="{{$data->harga}}"
                            data-jumlah="{{$data->jumlah}}"><em class="icon ni ni-eye"></em>
                            VIEW
                        </a>
                    </td>
                    <td>
                        <a class="btn btn-warning" id="set_dtl_data" data-toggle="modal" data-target="#modal_edit"
                            data-id_produk="{{$data->id}}" data-nama_produk="{{$data->nama_produk}}"
                            data-keterangan="{{$data->keterangan}}" data-harga="{{$data->harga}}"
                            data-jumlah="{{$data->jumlah}}"><em class="icon ni ni-edit"></em>
                            EDIT
                        </a>
                    </td>
                    <td>
                        <a class="btn btn-danger" href="/delete_data/{{$data->id}}" onclick="alert_delete()"><em
                                class="icon ni ni-trash"></em>
                            DELETE
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="add_modal_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ADD DATA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/add_data" method="POST">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="username">NAMA PRODUK</label>
                        <div class="form-control-wrap">
                            <input type="text" name="nama_produk" class="form-control form-control-lg" id="username"
                                placeholder="Masukkan Nama Produk" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="username">KETERANGAN</label>
                        <div class="form-control-wrap">
                            <input type="text" name="keterangan" class="form-control form-control-lg" id="username"
                                placeholder="Masukkan Keterangan" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="form-label" for="harga">Harga Produk</label>
                            <div class="form-control-wrap">
                                <div class="form-text-hint">
                                    <span class="overline-title">Rp</span>
                                </div>
                                <input type="number" name="harga" class="form-control form-control-lg" id="nama_depan"
                                    placeholder="Masukkan harga produk" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="form-label" for="jumlah">Jumlah Produk</label>
                            <div class="form-control-wrap">
                                <input type="number" name="jumlah" class="form-control form-control-lg"
                                    id="nama_belakang" placeholder="Masukkan jumlah">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/update_data" method="POST">
                    @csrf

                    <div class="form-group">
                        <label class="form-label" for="username">NAMA PRODUK</label>
                        <div class="form-control-wrap">
                            <input type="hidden" name="id_produk_edit" value="" class="form-control form-control-lg"
                                id="username" placeholder="Masukkan Nama Produk" required>

                            <input type="text" name="nama_produk_edit" value="" class="form-control form-control-lg"
                                id="username" placeholder="Masukkan Nama Produk" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="username">KETERANGAN</label>
                        <div class="form-control-wrap">
                            <input type="text" name="keterangan_edit" value="" class="form-control form-control-lg"
                                id="username" placeholder="Masukkan Keterangan" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="form-label" for="harga">Harga Produk</label>
                            <div class="form-control-wrap">
                                <div class="form-text-hint">
                                    <span class="overline-title">Rp</span>
                                </div>
                                <input type="number" name="harga_edit" class="form-control form-control-lg" value=""
                                    id="nama_depan" placeholder="Masukkan harga produk" required>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="form-label" for="jumlah">Jumlah Produk</label>
                            <div class="form-control-wrap">
                                <input type="number" name="jumlah_edit" class="form-control form-control-lg"
                                    id="nama_belakang" value="" placeholder="Masukkan jumlah">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/update_data" method="POST">
                    @csrf

                    <div class="form-group">
                        <label class="form-label" for="username">NAMA PRODUK</label>
                        <div class="form-control-wrap">
                            <input type="hidden" name="id_produk_view" value="" class="form-control form-control-lg"
                                id="username" placeholder="Masukkan Nama Produk" readonly>

                            <input type="text" name="nama_produk_view" value="" class="form-control form-control-lg"
                                id="username" placeholder="Masukkan Nama Produk" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="username">KETERANGAN</label>
                        <div class="form-control-wrap">
                            <input type="text" name="keterangan_view" value="" class="form-control form-control-lg"
                                id="username" placeholder="Masukkan Keterangan" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label class="form-label" for="harga">Harga Produk</label>
                            <div class="form-control-wrap">
                                <div class="form-text-hint">
                                    <span class="overline-title">Rp</span>
                                </div>
                                <input type="number" name="harga_view" class="form-control form-control-lg" value=""
                                    id="nama_depan" placeholder="Masukkan harga produk" readonly>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="form-label" for="jumlah">Jumlah Produk</label>
                            <div class="form-control-wrap">
                                <input type="number" name="jumlah_view" class="form-control form-control-lg"
                                    id="nama_belakang" value="" placeholder="Masukkan jumlah" readonly>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                {{-- <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button> --}}
            </div>
            </form>
        </div>
    </div>
</div>

{{-- DATA TABLE END --}}

@endsection

@section('scriptjs')
@include('sweetalert::alert');
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready( function () {
                // $('#myTable').DataTable();
                $('#myTable').DataTable( {
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    } );
            });
</script>
<script type="text/javascript">
    function alert_delete() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                    Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                    )}
                })

</script>
<script>
    $(document).on("click", "#set_dtl_data", function() {
        var id_produk = $(this).data("id_produk");
            var nama_produk = $(this).data("nama_produk");
            var keterangan = $(this).data("keterangan");
            var harga = $(this).data("harga");
            var jumlah = $(this).data("jumlah");
            $("[name='id_produk_edit']").prop("value", id_produk);
            $("[name='nama_produk_edit']").prop("value", nama_produk);
            $("[name='keterangan_edit']").prop("value", keterangan);

            $("[name='harga_edit']").prop("value", harga);
            $("[name='jumlah_edit']").prop("value", jumlah);

            console.log(
                nik_pengguna,
                nama_pengguna,
                grupcg_pengguna,
                dept,
                username,
                status,
                level,
                nik_pengguna,
            );

        });
</script>
<script>
    $(document).on("click", "#set_dtl_data_view", function() {
        var id_produk = $(this).data("id_produk");
            var nama_produk = $(this).data("nama_produk");
            var keterangan = $(this).data("keterangan");
            var harga = $(this).data("harga");
            var jumlah = $(this).data("jumlah");
            $("[name='id_produk_view']").prop("value", id_produk);
            $("[name='nama_produk_view']").prop("value", nama_produk);
            $("[name='keterangan_view']").prop("value", keterangan);

            $("[name='harga_view']").prop("value", harga);
            $("[name='jumlah_view']").prop("value", jumlah);

            console.log(
                nik_pengguna,
                nama_pengguna,
                grupcg_pengguna,
                dept,
                username,
                status,
                level,
                nik_pengguna,
            );

        });
</script>
@endsection
