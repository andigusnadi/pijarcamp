<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use App\Models\m_pengguna;
use App\Models\m_produk;



class DataController extends Controller
{
    public function index()
    {
        $produk = DB::table('produk')->get();
        return view('admin.data.v_data', ['ListProduk' => $produk]);
    }

    public function action_add(Request $request)
    {
        $data_input = array(
            'nama_produk' => $request->input('nama_produk'),
            'keterangan' => $request->input('keterangan'),
            'harga' => $request->input('harga'),
            'jumlah' => $request->input('jumlah'),
        );

        $rules = array(
            'nama_produk' => 'required',
            'keterangan' => 'required',
            'harga' => 'required',
            'jumlah' => 'required',
        );

        $validator = Validator::make($data_input, $rules);

        if ($validator->fails()) {

            return redirect()->back()->with('errors', $validator->messages()->all()[0])->withInput();
        } else {
            $add_data = m_produk::create([
                'nama_produk' => $request->nama_produk,
                'keterangan' => $request->keterangan,
                'harga' => $request->harga,
                'jumlah' => $request->jumlah,

            ]);

            return redirect('/')->with('success', 'Data Berhasil Di Input!');
        }
    }

    public function action_update(Request $request)
    {
        $data_input = array(
            'nama_produk' => $request->input('nama_produk_edit'),
            'keterangan' => $request->input('keterangan_edit'),
            'harga' => $request->input('harga_edit'),
            'jumlah' => $request->input('jumlah_edit'),
        );

        $rules = array(
            'nama_produk' => 'required',
            'keterangan' => 'required',
            'harga' => 'required',
            'jumlah' => 'required',
        );

        $validator = Validator::make($data_input, $rules);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->messages()->all()[0])->withInput();
        } else {

                $update_opl = DB::table('produk')
                ->where('id', $request->id_produk_edit)
                ->update([
                    'nama_produk' => $request->nama_produk_edit,
                    'keterangan' => $request->keterangan_edit,
                    'harga' => $request->harga_edit,
                    'jumlah' => $request->jumlah_edit,
                ]);
            

            return redirect('/')->with('success', 'Data Berhasil Di Update!');
        }
    }

    public function action_delete($id)
    {

        DB::table('produk')->where('id', $id)->delete();
        return redirect('/')->with('success', 'Data Berhasil Di Hapus!');
    }

    
}
