<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_produk extends Model
{
    use HasFactory;

    protected $table = 'produk'; // MASUKIN NAMA TABLE NYA
    protected $primaryKey = 'id'; //PRIMARY KEY NYA APA
    public $timestamps = true;  //TIMESTAMP NYA DI OFF KARENA TIDAK PAKE


    protected $fillable = [
        'nama_produk',
        'keterangan',
        'nama_belakang',
        'harga',
        'jumlah',
        'created_at',
        'updated_at'
    ];
}
