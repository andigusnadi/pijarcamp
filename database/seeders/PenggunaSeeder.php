<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_pengguna')->insert([

            'nama_depan' => 'admin',
            'nama_belakang' => 'admin',
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'email' => 'admin' . '@gmail.com',
            'status' => 'Active',
            'role' => 'Admin',
            'no_kontak' => '08123456789',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
